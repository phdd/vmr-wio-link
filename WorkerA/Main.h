#ifndef __MAIN_H__
#define __MAIN_H__
#include "suli2.h"
#include "grove_airquality_tp401a_gen.h"
#include "grove_digital_light_gen.h"

extern GroveAirquality *GroveAirqualityA0_ins;
extern GroveDigitalLight *GroveDigitalLightI2C0_ins;
#endif
