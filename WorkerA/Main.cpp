#include "wio.h"
#include "suli2.h"
#include "Main.h"

int getAirQuality(GroveAirquality *groveAirquality) {
    int value;
    static int lastValue = -1000000;
    bool isValid = groveAirquality->read_quality(&value);

    if (isValid && 0 < value && value < 10000)
        return lastValue = value;
    else
        return lastValue;
}

uint32_t getAmbientLight(GroveDigitalLight *groveDigitalLight) {
    uint32_t value;
    static uint32_t lastValue = -1000000;
    bool isValid = groveDigitalLight->read_lux(&value);

    if (isValid && 0 < value && value < 40000)
        return lastValue = value;
    else
        return lastValue;
}

// Application

uint32_t lastTime;

void setup() {
    lastTime = millis();
}

void loop() {
    uint32_t currentTime = millis();

    if (currentTime - lastTime > 100) {
        lastTime = currentTime;

        wio.postEvent("airQuality", getAirQuality(GroveAirqualityA0_ins));
        wio.postEvent("ambientLight", getAmbientLight(GroveDigitalLightI2C0_ins));
    }
}
