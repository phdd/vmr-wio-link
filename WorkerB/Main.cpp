#include "wio.h"
#include "suli2.h"
#include "Main.h"

int getLoudness(GroveLoudness *groveLoudness) {
    int value;
    static int lastValue = -1000000;
    bool isValid = groveLoudness->read_loudness(&value);

    if (isValid && -1 < value && value < 1024)
        return lastValue = value;
    else
        return lastValue;
}

float getTemperature(GroveTempHumPro *groveTempHum) {
    float value;
    static float lastValue = -1000000.0;
    bool isValid = groveTempHum->read_temperature(&value);

    if (isValid && -100 < value && value < 1000)
        return lastValue = value;
    else
        return lastValue;
}

float getHumidity(GroveTempHumPro *groveTempHum) {
    float value;
    static float lastValue = -1000000.0;
    bool isValid = groveTempHum->read_humidity(&value);

    if (isValid && 0 < value && value <= 100)
        return lastValue = value;
    else
        return lastValue;
}

// Application

uint32_t lastTime;

void setup() {
    lastTime = millis();
}

void loop() {
    uint32_t currentTime = millis();

    if (currentTime - lastTime > 100) {
        lastTime = currentTime;

        wio.postEvent("loudness", getLoudness(GroveLoudnessA0_ins));
        wio.postEvent("temperature", getTemperature(GroveTempHumProD0_ins));
        wio.postEvent("humidity", getHumidity(GroveTempHumProD0_ins));
    }
}
