RESTClient = require('node-rest-client').Client
cli = require('vorpal')()
flatCache = require 'flat-cache'
cache = flatCache.load 'wio-settings'
colors = require 'colors'
fs = require 'q-io/fs'
path = require 'path'
_ = require 'lodash'
q = require 'q'

####################### Prep

settings =
  address: 'http://192.168.12.17:8080'
  token: null

selectedDevice = null

wio = new RESTClient()

wio.registerMethod 'login',        '${host}/v1/user/login',   'POST'
wio.registerMethod 'list',         '${host}/v1/nodes/list',   'GET'
wio.registerMethod 'deviceConfig', '${host}/v1/node/config',  'GET'
wio.registerMethod 'downloadULB',  '${host}/v1/cotf/project', 'GET'
wio.registerMethod 'uploadULB',    '${host}/v1/cotf/project', 'POST'
wio.registerMethod 'triggerOTA',   '${host}/v1/ota/trigger',  'POST'
wio.registerMethod 'OTAStatus',    '${host}/v1/ota/status',   'GET'

containing = (params) ->
  args =
    path:
      host: settings.address
    headers:
      Authorization: "token #{settings.token}"
      'Content-Type': 'application/json'
    parameters: params
    data: params.body

  params.access_token = selectedDevice.node_key if selectedDevice? and selectedDevice != null

  delete args.headers.Authorization if settings.token == null
  delete params.body if params.body?
  delete args.data if not args.data?

  args

withoutParameters = -> containing {}

asUser = (params) ->
  args = containing params
  delete args.parameters.access_token if args.parameters.access_token?
  args

asUserWithoutParameters = ->
  args = withoutParameters()
  delete args.parameters.access_token if args.parameters.access_token?
  args

save = ->
  for key in Object.keys(settings)
    cache.setKey key, settings[key]
  cache.save()

for key in cache.keys()
  settings[key] = cache.getKey key

####################### Business

userIsAuthorized = () ->
  deferred = q.defer()

  if settings.token != null then deferred.resolve()
  else deferred.reject 'You have to login'

  deferred.promise

deviceHasBeenSelected = () ->
  deferred = q.defer()

  if selectedDevice != null then deferred.resolve()
  else deferred.reject 'You have to select a device'

  deferred.promise

login = (email, password) ->
  deferred = q.defer()
  data = containing
      email: email
      password: password

  delete data.headers
  wio.methods.login data, (data) =>
      if data.error? then deferred.reject data.error
      else if data.token? then deferred.resolve data.token
      else deferred.reject "Unknown response: #{JSON.stringify data}"

  deferred.promise

listDevices = ->
  deferred = q.defer()

  wio.methods.list asUserWithoutParameters(), (data) =>
    if data.error? then deferred.reject data.error
    else if data.nodes? then deferred.resolve data.nodes
    else deferred.reject "Unknown response: #{JSON.stringify data}"

  deferred.promise

findDeviceBy = (deviceName) ->
  listDevices()
    .then (devices) -> # TODO MAYBE _.find http://blurback.com/post/101631242892/promise-chains-made-pretty-with-lodash
      deferred = q.defer()
      for device in devices
        if device.name is deviceName
          deferred.resolve device
          return deferred.promise

      deferred.reject 'Device is not on the list'
      deferred.promise
    .then (device) ->
      deferred = q.defer()
      if device.online then deferred.resolve device
      else deferred.reject 'Device is offline'
      deferred.promise

downloadULB = (context) ->
  deferred = q.defer()

  wio.methods.downloadULB withoutParameters(), (data) =>
      if data.error? then deferred.reject data.error
      else if data['./Main.h']? then deferred.resolve data
      else deferred.reject "Unknown response: #{JSON.stringify data}"

  deferred.promise

uploadULB = (ulb) ->
  deferred = q.defer()

  if not _.isEmpty ulb
    wio.methods.uploadULB containing(
        body: ulb
      ), (data) =>
        if data.result? and data.result is 'ok' then deferred.resolve()
        else if data.error? then deferred.reject data.error
        else deferred.reject "Unknown response: #{JSON.stringify data}"
  else deferred.reject 'ULB is empty'

  deferred.promise

triggerOTA = (config) ->
  deferred = q.defer()

  wio.methods.triggerOTA containing(
      body: config
    ), (data) =>
      if data.ota_status? is 'going' then deferred.resolve()
      else if data.ota_msg? then deferred.reject data.ota_msg
      else deferred.reject "Unknown response: #{JSON.stringify data}"

  deferred.promise

fetchDeviceConfig = ->
  deferred = q.defer()

  wio.methods.deviceConfig withoutParameters(), (data) =>
    if data.config? then deferred.resolve data.config
    else deferred.reject "Unknown response: #{JSON.stringify data}"

  deferred.promise

writeULB = (ulb, directory) ->
  fs.makeTree directory
    .then ->
      _.map ulb, (content, file) ->
        name: "#{fs.join(directory, file)}"
        source: content
    .then (sources) ->
      _.map sources, (file) ->
        fs.write file.name, file.source

readULB = (directory) ->
  fs.listTree directory, (path, stat) ->
      not stat.isDirectory()
    .then (paths) ->
      contents = _.map paths, (path) -> fs.read path
      files = []

      _.each paths, (path, index) ->
        files.push path.replace RegExp("#{directory}\/?"), './'
        files.push contents[index]

      q.all files
    .then (files) -> _.chunk files, 2
    .then (files) -> _.fromPairs files

####################### CLI

cli.command 'address [address]', 'Get/set the Wio server address'
  .action (args, callback) ->
    if not args.address?
      @log "⇒ Wio server address is '#{settings.address}'"
    else
      settings.address = args.address; save()
      @log "⇒ Wio server address set to '#{settings.address}'"
    callback()

cli.command 'login <email>', 'Login on the Wio-Server'
  .action (args, callback) ->
    @prompt
        type: 'password',
        name: 'password',
        message: 'password: '
      .then (input) -> login args.email, input.password
      .then (token) => settings.token = token; save()
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli.command 'list', 'List all devices'
  .action (args, callback) ->
    userIsAuthorized()
      .then listDevices
      .then (devices) =>
        for device in devices
          state = if device.online then '✓'.green else '✗'.red
          @log "#{state} #{device.name} (#{device.node_key})"
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli.command 'select <deviceName>', 'Select a device by its name'
  .action (args, callback) ->
    userIsAuthorized()
      .then -> findDeviceBy args.deviceName
      .then (device) =>
        selectedDevice = device; save()
        delimiter = "wio:#{device.name}: "

        cli.delimiter delimiter
        cli.ui.delimiter delimiter
        cli.ui.refresh()
      .catch (error) => @log error
      .then -> callback()

cli.command 'ulb download <directory>', 'Download User Logic Block from selected device to directory'
  .action (args, callback) ->
    userIsAuthorized()
      .then deviceHasBeenSelected
      .then downloadULB
      .then (ulb) => writeULB ulb, args.directory
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli.command 'ulb upload <directory>', 'Upload User Logic Block from directory to selected device'
  .action (args, callback) ->
    userIsAuthorized()
      .then deviceHasBeenSelected
      .then -> readULB args.directory
      .then uploadULB
      .then fetchDeviceConfig
      .then triggerOTA
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli.command 'config download <file>', 'Download device configuration from selected device to file'
  .action (args, callback) ->
    userIsAuthorized()
      .then deviceHasBeenSelected
      .then fetchDeviceConfig
      .then (config) =>
        fs.write args.file, JSON.stringify(config, null, 2)
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli.command 'config upload <file>', 'Upload device configuration from file to selected device'
  .action (args, callback) ->
    userIsAuthorized()
      .then deviceHasBeenSelected
      .then (config) => fs.read args.file
      .then triggerOTA
      .catch (error) => @log "⇒ #{error}"
      .then -> callback()

cli
  .history '.wio-history'
  .delimiter 'wio:'
  .show()
